package com.betternowapps.calendar.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.betternowapps.calendar.MainActivity
import com.betternowapps.calendar.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val intent: Intent = Intent(applicationContext, MainActivity::class.java);
        startActivity(intent);
        finish();
    }
}
