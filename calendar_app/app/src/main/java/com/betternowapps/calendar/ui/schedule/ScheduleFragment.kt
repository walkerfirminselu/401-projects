package com.betternowapps.calendar.ui.schedule

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.betternowapps.calendar.R
import com.betternowapps.calendar.db.EventDao
import com.betternowapps.calendar.repository.EventRepository

class ScheduleFragment : Fragment() {

    private lateinit var scheduleViewModel: ScheduleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        scheduleViewModel = ViewModelProviders.of(this).get(ScheduleViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_schedule, container, false)
//        val list = scheduleViewModel.allEvents.value
//        Log.d("debug log: ", "this is a message")
//        list?.forEach { Log.d("debug log:","This is an event ${it.title} ${it.description} ${it.date}") }
        return root
    }
}