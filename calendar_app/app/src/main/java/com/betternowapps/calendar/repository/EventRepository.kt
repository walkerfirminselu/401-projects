package com.betternowapps.calendar.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.betternowapps.calendar.db.Event
import com.betternowapps.calendar.db.EventDao

class EventRepository(private val eventDao: EventDao) {

    private val allEvents: LiveData<List<Event>> = eventDao.getAllEvents()

    fun insert(event: Event) {
        InsertEventAsyncTask(
            eventDao
        ).execute(event)
    }

    fun update(event: Event) {
        UpdateEventAsyncTask(eventDao).execute(event)
    }

    fun delete(eventId: Int) {
        DeleteEventAsyncTask(
            eventDao
        ).execute(eventId)
    }

    fun deleteAllEvents() {
        DeleteAllEventsAsyncTask(
            eventDao
        ).execute()
    }

    fun getAllEvents(): LiveData<List<Event>> {
        return allEvents
    }

    private class InsertEventAsyncTask(val eventDao: EventDao) : AsyncTask<Event, Unit, Unit>() {

        override fun doInBackground(vararg event: Event?) {
            eventDao.insert(event[0]!!)
        }
    }

    private class UpdateEventAsyncTask(val eventDao: EventDao) : AsyncTask<Event, Unit, Unit>() {

        override fun doInBackground(vararg event: Event?) {
            eventDao.update(event[0]!!)
        }
    }

    private class DeleteEventAsyncTask(val eventDao: EventDao) : AsyncTask<Int, Unit, Unit>() {

        override fun doInBackground(vararg eventId: Int?) {
            eventDao.deleteEvent(eventId[0]!!)
        }
    }


    private class DeleteAllEventsAsyncTask(val eventDao: EventDao) : AsyncTask<Unit, Unit, Unit>() {

        override fun doInBackground(vararg p0: Unit?) {
            eventDao.deleteAllEvents()
        }
    }

}