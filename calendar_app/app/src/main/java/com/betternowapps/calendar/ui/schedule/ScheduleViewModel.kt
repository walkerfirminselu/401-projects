package com.betternowapps.calendar.ui.schedule

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.betternowapps.calendar.db.Event
import com.betternowapps.calendar.db.EventDatabase
import com.betternowapps.calendar.repository.EventRepository

class ScheduleViewModel(application: Application) : AndroidViewModel(application) {

    val title: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val allEvents: LiveData<List<Event>>

    private val repository: EventRepository

    init {
        val eventDao = EventDatabase.getInstance(application).eventDao()
        repository = EventRepository(eventDao)
        allEvents = repository.getAllEvents()
    }

    fun insert(event: Event) {
        repository.insert(event)
    }

    fun update(event: Event) {
        repository.update(event)
    }

    fun delete(eventId: Int) {
        repository.delete(eventId)
    }

    fun deleteAllEvents() {
        repository.deleteAllEvents()
    }
}