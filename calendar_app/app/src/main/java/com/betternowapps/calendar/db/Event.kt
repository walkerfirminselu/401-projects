package com.betternowapps.calendar.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*
@Entity(tableName = "Events")
data class Event(@PrimaryKey(autoGenerate = true)var id: Int,var title: String, var description: String, val date: Date)