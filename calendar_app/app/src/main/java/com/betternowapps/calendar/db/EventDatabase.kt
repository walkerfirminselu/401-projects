package com.betternowapps.calendar.db

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.betternowapps.calendar.converters.Converters
import java.util.*


@Database(entities = [Event::class], version = 1)
@TypeConverters(Converters::class)
abstract class EventDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDao


    companion object {
        private var instance: EventDatabase? = null

        fun getInstance(context: Context): EventDatabase {
            if (instance == null) {
                synchronized(EventDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        EventDatabase::class.java, "event_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                PopulateDbAsyncTask(instance)
                    .execute()
            }
        }

    }
    class PopulateDbAsyncTask(db: EventDatabase?) : AsyncTask<Unit, Unit, Unit>() {
        private val eventDao = db?.eventDao()

        override fun doInBackground(vararg p0: Unit?) {
            eventDao?.insert(Event(1, "first eventDao","it's jakes birthday", Date(Date().time)))
            eventDao?.insert(Event(2, "second eventDao","it's jakes birthday", Date(Date().time)))
            eventDao?.insert(Event(3, "third eventDao","it's jakes birthday", Date(Date().time)))
        }
    }

}