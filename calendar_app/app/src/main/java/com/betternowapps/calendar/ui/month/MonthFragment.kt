package com.betternowapps.calendar.ui.month

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.betternowapps.calendar.R
import com.betternowapps.calendar.ui.create_event.CreateEvent
import ru.cleverpumpkin.calendar.CalendarDate
import ru.cleverpumpkin.calendar.CalendarView
import java.util.*

class MonthFragment : Fragment() {

    private lateinit var homeViewModel: MonthViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(MonthViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_month, container, false)
        val calendarView: CalendarView = root.findViewById(R.id.calendar_view)
        val calendar = Calendar.getInstance()
        val preselectedDates = mutableListOf<CalendarDate>()

        // Initial date
        calendar.set(2018, Calendar.JUNE, 1)
//        preselectedDates += CalendarDate(calendar.time)
        val initialDate = CalendarDate(calendar.time)

        // Minimum available date
        calendar.set(2018, Calendar.MAY, 15)
        val minDate = CalendarDate(calendar.time)
//        preselectedDates += CalendarDate(calendar.time)

        // Maximum available date
        calendar.set(2018, Calendar.JULY, 15)
        val maxDate = CalendarDate(calendar.time)
//        preselectedDates += CalendarDate(calendar.time)

        calendarView.setupCalendar()
        calendarView.onDateClickListener = {
            Toast.makeText(this.context,"The date ${it.toString()}",Toast.LENGTH_SHORT).show()
            val intent: Intent = Intent(context, CreateEvent::class.java)
            var bundle: Bundle
//            bundle.putSerializable("DATE",it.)
//            intent.putExtras()
            startActivityForResult(intent,1)
        }
        return root
    }
}

class clickButton(val view: View): RecyclerView.ViewHolder(view)
{
    init {
        view.setOnClickListener{
            println("TRUE")
        }
    }

}