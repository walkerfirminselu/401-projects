package com.betternowapps.calendar.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface EventDao {

    @Insert
    fun insert(event: Event)

    @Update
    fun update(event: Event)

    @Query("SELECT * FROM Events WHERE id == :id")
    fun deleteEvent(id: Int)

    @Query("DELETE FROM Events")
    fun deleteAllEvents()

    @Query("SELECT * FROM events")
    fun getAllEvents():LiveData<List<Event>>
}