package com.betternowapps.calendar.ui.create_event

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.LinearLayoutCompat
import com.betternowapps.calendar.R
import kotlinx.android.synthetic.main.activity_create_event.*
import java.util.*

class CreateEvent : AppCompatActivity() {

    companion object {
        const val EXTRA_TITLE = "com.betternowapps.calendar.EXTRA_TITLE"
        const val EXTRA_DESCRIPTION = "com.betternowapps.calendar.EXTRA_DESCRIPTION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_event)
        val tVStartTime = findViewById<LinearLayoutCompat>(R.id.tVStartTime)
        val startTime     = findViewById<TextView>(R.id.startTime)
        val tVEndTime = findViewById<LinearLayoutCompat>(R.id.tvEndTime)
        val endTime     = findViewById<TextView>(R.id.endTime)
        val tVStartDate = findViewById<LinearLayoutCompat>(R.id.tVStartDate)
        val startDate     = findViewById<TextView>(R.id.startDate)
        val tVEndDate = findViewById<LinearLayoutCompat>(R.id.tVEndDate)
        val endDate     = findViewById<TextView>(R.id.endDate)
        setTime(tVStartTime,startTime)
        setTime(tVEndTime,endTime)
        setDate(tVStartDate,startDate)
        setDate(tVEndDate,endDate)
    }

    fun setTime(layout: LinearLayoutCompat,time: TextView){
        layout.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                time.text = SimpleDateFormat("HH:mm").format(cal.time)
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }
    }

//    fun ReturnValues()
//    {
//        currentInstanceHour = Calendar.HOUR_OF_DAY, hour;
//        currentInstanceMinute = Calendar.MINUTE, minute;
//        if (cal.isEmpty = false)
//        {
//            MonthDay1 = ("THe date and time of the  event is" + currentInstanceHOur + currentInstanceMinute)
//
//            calendarDay.push(MonthDay1 + (xml identifier))
//        }
//
//    }



    fun setDate(layout: LinearLayoutCompat,time: TextView){
        layout.setOnClickListener {
            val cal = Calendar.getInstance()
            val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                cal.set(Calendar.YEAR,year)
                cal.set(Calendar.MONTH,month)
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
            val  myFormat = "dd.MM.yyyy"
            val sdf: SimpleDateFormat = SimpleDateFormat(myFormat, Locale.US)
                time.text = sdf.format(cal.time)
            }
            DatePickerDialog(this@CreateEvent,dateSetListener,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_event_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return super.onOptionsItemSelected(item)
        return when (item?.itemId) {
            R.id.save_event -> {
                saveEvent()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveEvent() {
        if (TitleText.text.toString().trim().isBlank() || eTDescription.text.toString().trim().isBlank()) {
            Toast.makeText(this, "Can not insert empty event!", Toast.LENGTH_SHORT).show()
            return
        }

        val data = Intent().apply {
            putExtra(EXTRA_TITLE, TitleText.text.toString())
            putExtra(EXTRA_DESCRIPTION, eTDescription.text.toString())
        }
        setResult(Activity.RESULT_OK, data)
        finish()
    }


}
