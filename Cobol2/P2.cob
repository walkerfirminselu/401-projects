

					IDENTIFICATION DIVISION.
					PROGRAM-ID.  P2.
                    *> Used to specify I/O files
					ENVIRONMENT DIVISION.
					INPUT-OUTPUT SECTION.
					FILE-CONTROL.
                        SELECT inFile  ASSIGN TO "P2In.dat" ORGANIZATION IS LINE SEQUENTIAL.
						SELECT outFile ASSIGN TO "P2Out.dat" ORGANIZATION IS LINE SEQUENTIAL.
					DATA DIVISION.
                    *> Used to describe record structure of the file
					FILE SECTION.
					FD inFile.
                    01 inputRecord.
                       05  Course  PIC X(8).
                       05  FILLER  PIC X(12).
                       05  Title    PIC X(30).
                       05  FILLER  PIC X(6).
                       05  GR   PIC X(3).
                       05  FILLER  PIC X(5).
                       05  Credit   PIC X(3).

                   FD outFile.
                   01 outputRecord.
                       05  Course  PIC X(8).
                       05  FILLER  PIC X(12).
                       05  Title    PIC X(30).
                       05  FILLER  PIC X(6).
                       05  GR   PIC X(1).
                       05  FILLER  PIC X(4).
                       05  Earned   PIC X(4).
                       05  FILLER  PIC X(4).
                       05  QPTS PIC X(6).
            *>    Stores temporary files used in the program
					WORKING-STORAGE SECTION.
					01 w  PIC X(3)  VALUE "Yes".
                    01 WS-UNI-NAME-TOP.
                        05 FILLER      PIC X(20).
                        05 WS-UNI-NAME PIC X(34) VALUE "SOUTHEASTERN LOUISIANNA UNIVERSITY".
                        05 FILLER      PIC X(126).
                    01 WS-UNI-ADD-TOP. 
                        05 FILLER      PIC X(30).
                        05 WS-UNI-ADD  PIC X(17) VALUE "HAMMOND, LA 70402".
                        05 FILLER      PIC X(133).
                    *> Stores files that are allocated and initialized at program start
                    local-storage section.
                    01 num PIC 9(3).
                    01 grade PIC A.
                    01 cellQPTS PIC 9(3).
                    01 cellEarned PIC 9(2).
                    01 semesterEarned PIC 9(3).
                    01 semesterQPTS PIC 9(3).
                    01 cumulativeQPTS PIC 9(3).
                    01 cumulativeEarned PIC 9(3).
                    01 semesterString PIC X(74) VALUE "                           SEMESTER                            14       43".
                    01 cumulativeString PIC X(74) VALUE "                           CUMULATIVE                          14       43".
                   *>    Describes variables from external program
                    linkage section.
                    
                    
					PROCEDURE DIVISION.
                    OPEN INPUT inFile.
                    OPEN OUTPUT outFile.
                    PERFORM printHeading 1 TIMES.
                    PERFORM readInFile UNTIL w = "NO".
                    MOVE cumulativeEarned to cumulativeString(64:3)
                    MOVE cumulativeQPTS to cumulativeString(72:3)
                    MOVE semesterEarned to semesterString(64:3)
                    MOVE semesterQPTS to semesterString(72:3)
                    PERFORM printSemester
                    PERFORM printCumulative
                    CLOSE inFile.
                    CLOSE outFile.
                    STOP RUN.
                    
                    readInFile.
                    READ inFile NEXT RECORD INTO outputRecord
						   AT END 
							  MOVE "NO" TO w
						   NOT AT END
                              MOVE outputRecord(57:3) TO num
                              MOVE outputRecord(65:1) TO cellEarned
                              PERFORM findGrade
                              MOVE grade to outputRecord(57:3)
                              MOVE cellEarned to outputRecord(65:3)
                              MOVE cellQPTS to outputRecord(72:3)
                              ADD cellQPTS TO cumulativeQPTS
                              DISPLAY outputRecord
                              WRITE outputRecord
						END-READ.

                    printHeading.
                       DISPLAY "Jake".
                       Move "Jake" to outputRecord
                       WRITE outputRecord
                       DISPLAY "w0000000".
                       Move "w0000000" to outputRecord
                       WRITE outputRecord
                       DISPLAY WS-UNI-NAME-TOP.
                       Move WS-UNI-NAME-TOP to outputRecord
                       WRITE outputRecord
                       DISPLAY WS-UNI-ADD-TOP.
                       Move WS-UNI-ADD-TOP to outputRecord
                       WRITE outputRecord
                       DISPLAY "COURSE              TITLE                               GR   EARNED    QPTS".
                       Move "COURSE              TITLE                               GR   EARNED    QPTS" to outputRecord
                       WRITE outputRecord.

                   findGrade.
                       IF num < 60 THEN
                       MOVE 'F' to grade
                       MULTIPLY cellEarned BY 0 GIVING cellQPTS
                       MOVE 0 to cellEarned
                       ADD cellEarned to semesterEarned
                       ADD cellQPTS to semesterQPTS
                       ADD cellEarned TO cumulativeEarned
                       END-IF.
                       IF num NOT < 60 AND NOT > 70 THEN
                       MOVE 'D' to grade
                       MULTIPLY cellEarned BY 1 GIVING cellQPTS
                       ADD cellEarned to semesterEarned
                       ADD cellQPTS to semesterQPTS
                       ADD cellEarned TO cumulativeEarned.
                       IF num NOT < 70 AND NOT > 80 THEN
                       MOVE 'C' to grade
                       MULTIPLY cellEarned BY 2 GIVING cellQPTS
                       ADD cellEarned to semesterEarned
                       ADD cellQPTS to semesterQPTS
                       ADD cellEarned TO cumulativeEarned.
                       IF num NOT < 80 AND NOT > 90 THEN
                       MOVE 'B' to grade
                       MULTIPLY cellEarned BY 3 GIVING cellQPTS
                       ADD cellEarned to semesterEarned
                       ADD cellQPTS to semesterQPTS
                       ADD cellEarned TO cumulativeEarned.
                       IF num NOT < 90 AND NOT > 100 THEN
                       MOVE 'A' to grade
                       MULTIPLY cellEarned BY 4 GIVING cellQPTS
                       ADD cellEarned to semesterEarned
                       ADD cellQPTS to semesterQPTS
                       ADD cellEarned TO cumulativeEarned.

                   printSemester.
                       DISPLAY semesterString.

                   printCumulative.
                       DISPLAY cumulativeString.

