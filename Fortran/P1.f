!Alex Kirkpatrick, Jake Clayton, Walker Firmin, Matthew Renshaw
!Fortran tutorials at https://www.tutorialspoint.com/fortran/ used for reference
      program P1
      implicit none

!Program variables
      	integer :: switch = 1
	real :: input, output

!Function return type declarations	
	real :: kilograms, pounds, meters, feet, celsius, fahrenheit

!do while loop to repeat program until prompted to exit
!print statements display program instructions to user
	do while (switch /= 0)
		print *,"------------------------------------------------------"
		print *,"Enter number of desired conversion (1-6) or 0 to exit."
		print *,"------------------------------------------------------"
		print *,"(1) Pounds to Kilograms"
		print *,"(2) Kilograms to Pounds"
		print *,"(3) Feet to Meters"
		print *,"(4) Meters to Feet"
		print *,"(5) Fahrenheit to Celsius"
		print *,"(6) Celsius to Fahrenheit"
		print *,"(0) Exit program"
		print *,"------------------------------------------------------"

		read *, switch
		print *,"------------------------------------------------------"

!select case to branch depending on user input
		select case (switch)
!case 1 prompts for input of pounds, calls kilograms function
			case (1)
				print *,"Enter pounds to convert:"
				read *, input
				output = kilograms(input)
				print *,input,"pounds converts into"
				print *,output,"kilograms."			
!case 2 prompts for input of kilograms, calls pounds function
			case (2)
				print *,"Enter kilograms to convert:"
				read *, input
				output = pounds(input)
				print *,input,"kilograms converts into"
				print *,output,"pounds."
!case 3 prompts for input of feet, calls meters function
			case (3)
				print *,"Enter feet to convert:"
				read *, input
				output = meters(input)
				print *,input,"feet converts into"
				print *,output,"meters."
!case 4 prompts for input of meters, calls feet function
			case (4)
				print *,"Enter meters to convert:"
				read *, input
				output = feet(input)
				print *,input,"meters converts into"
				print *,output,"feet."
!case 5 prompts for input of fahrenheit temperature, calls celsius function
			case (5)
				print *,"Enter Fahrenheit temperature to convert:"
				read *, input
				output = celsius(input)
				print *,input,"degrees Fahrenheit converts into"
				print *,output,"degrees Celsius."
!case 6 prompts for input of celsius temperature, calls fahrenheit function
			case (6)
				print *,"Enter Celsius temperature to convert:"
				read *, input
				output = fahrenheit(input)
				print *,input,"degrees Celsius converts into"
				print *,output,"degrees Fahrenheit."
!case 0 displays exit message, forces program to stop
			case (0)
				print *,"Exiting program..."
		print *,"------------------------------------------------------"
				stop
!case default handles all input that isn't specified, displays input error message
			case default
				print *,"Invalid input option. Please enter 1-6 or 0."
		end select
      	end do
      end program P1

!kilograms function converts input of pounds into kilograms
      function kilograms (lb)
      implicit none
	
	real :: kilograms, lb
	
	kilograms = lb * 0.45359237
      
      end function kilograms

!pounds function converts input of kilograms into pounds
      function pounds (kg)
      implicit none
	
	real :: pounds, kg

	pounds = kg / 0.45359237

      end function pounds

!meters function converts input of feet into meters
      function meters (ft)
      implicit none
	
	real :: meters, ft

	meters = ft * 0.3048

      end function meters

!feet function converts input of meters into feet
      function feet (m)
      implicit none

	real :: feet, m
	
	feet = m / 0.3048

      end function feet

!celsius function converts input of fahrenheit into celsius
      function celsius (f)
      implicit none

	real :: celsius, f

	celsius = (((f - 32) * 5 ) / 9)

      end function celsius

!fahrenheit function converts input of celsius into fahrenheit
      function fahrenheit (c)
      implicit none

	real :: fahrenheit, c

	fahrenheit = (((c * 9) / 5) + 32)

      end function fahrenheit
