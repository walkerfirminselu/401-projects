/*
    DATA TYPES AND VARIABLES
    var is used to make the variable mutable and val is to make a variable non-mutable
    Data types are inferred or specified by placing the following after
    : Int, : Long, : Short, : Byte,  : Double, : Float, : Char, : Boolean, : String, Array<T>
*/

fun main(){
    // Int
    var int: Int = 10
    println("\nInteger\nint + 1 is equal to ${int+1}")

    // Long
    var l: Long = Long.MAX_VALUE
    var l2 = 40L
    println("\nLong\nl is equal to ${l}\n and l2 is equal to $l2")

    // Short
    var s: Short = Short.MAX_VALUE
    println("\nShort\ns is equal to ${s}")

    // Byte
    var b: Byte = Byte.MAX_VALUE
    println("\nByte\nb is equal to ${b}")

    // Double
    var d: Double = Double.MAX_VALUE
    println("\nDouble\nd is equal to $d")

    // Float
    var f: Float = Float.MAX_VALUE
    var f2 = 40F
    println("\nFloat\nf is equal to $f\n and f2 is equal to $f2")

    val c : Char = 'A'
    println("\nChar\nc is equal to $c")

    val arr1 = arrayOf(1,2,3,"Apple")
    val arr2: Array<Int> = Array(6,{i -> i * 2})
    val arr3: Array<Int> = Array<Int>(6,{i -> i+1})
    val arr4 = arrayOf<Int>(1,2,3)
    val arrResult = arrayOf(arr1,arr2,arr3,arr4)
    print("\nArray\n")
    for (i in 0..3){
        println("Array $i is equal to "+java.util.Arrays.deepToString(arrResult[i]))
    }

    // Boolean
    val decision: Boolean = true;
    println("\nBoolean\nThe val decision is equal to ${decision}")

    // String
    val message: String = "Hello World!"
    println("\nString\nThe String message is "+message)

    // Triple
    val (age,animal,isAlive) = Triple(22, "Horse", true)
    println(age)

    // Null Safe
    // ? allows setting to null
    var a: String? = "abc"
    a = null
    // Says null is acceptable
    println(a?.length)
    // Throws error
//    println(a.length)
    // Throws exception
//    println(a!!.length)
    // Safe Cast
    a = "abc"
    val aInt: Int? = a as? Int
}

/*
OUTPUT
Integer
int + 1 is equal to 11

Long
l is equal to 9223372036854775807
 and l2 is equal to 40

Short
s is equal to 32767

Byte
b is equal to 127

Double
d is equal to 1.7976931348623157E308

Float
f is equal to 3.4028235E38
 and f2 is equal to 40.0

Char
c is equal to A

Array
Array 0 is equal to [1, 2, 3, Apple]
Array 1 is equal to [0, 2, 4, 6, 8, 10]
Array 2 is equal to [1, 2, 3, 4, 5, 6]
Array 3 is equal to [1, 2, 3]

Boolean
The val decision is equal to true

String
The String message is Hello World!

 */