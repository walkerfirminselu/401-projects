/*
SELECTION STATEMENTS (TSel.kt)
Test Selections: if, if-else, nested if-else
Logical Operators: &&, ||, !
Relational Operators: <, >, ==, >=, <=, !=
 */

fun main(){
    var i1 = 1; var i2: Int = 2; var i3: Int = 3; var i4: Int = 4; var i5: Int = 5; var i6: Int = 6

    // If else
    if(i1 < i2)
    {
        println("i1 < i2")
    }
    if(i1 < i2 && i2 < i1)
    {
        println("i1 < i2 && i2 < i1")
    }
    else
    {
        println("i1 < i2 && i2 > i1")
    }
    if(i1==i2-1 || i3==i4)
    {
        if (i5!=i6)
        {
            println("(i1==i2-1 || i3==i4) && i5!=i6")
        }
    }

    // Switch case
    when(60000) {
        0 -> println("I wont get called")
        1 -> println("I wont get called")
        2 -> println("The answer is $i2")
        in 3..6 -> println("Range of values")
        else -> println("I am the default")
    }

    when(null){
        true -> println("I am true")
        false -> println("I am false")
        else -> println("I am null")
    }
}

/*
OUTPUT
i1 < i2
i1 < i2 && i2 > i1
(i1==i2-1 || i3==i4) && i5!=i6

 */