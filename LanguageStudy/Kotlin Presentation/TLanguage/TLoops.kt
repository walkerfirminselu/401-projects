fun main()
{
    // Initializing Array
    val someArray = arrayOf(arrayOf(1,2,3), arrayOf(4,5,6), arrayOf("Bananna","Apple"))

    // Iterating array for loop
    for (row in someArray){
        for (value in row){
            print("$value ")
        }
        println()
    }

    // Iterating array while loop
    var index: Int = 0
    var arr = someArray[0]
    while (index < arr.size)
    {
        print(arr[index])
        index++
    }
    println()

    // Iterating Array using lambda expression with it parameter
    someArray[2].forEach {print("$it ")}
    println()

    // Iterating Array using lambda expression with named parameter
    someArray[2].forEach {myParm -> print("$myParm ")}
    println()
}