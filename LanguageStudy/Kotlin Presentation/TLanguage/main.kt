
    fun main(){
        var ans: Char
        var ansNum: Int = -1
        // Null variable initialization
        var pizzaChoice: String?

        // Array initialization
        val pizzas: Array<String> = arrayOf<String>("Neapolitan","Greek","Sushi","Tomato Pie","St. Louis Style")
        val toppings = arrayOf<String>("Mushrooms","Onion","Extra Cheese","Green Pepper","Feta","Bacon","Pepperoni")
        val choosen = mutableListOf<String>()

        while(true)
        {
            println("Welcome to Pao Pizza, can I take your order?\n(y/n)")
            // Not null check
            ans = readLine()!![0]
            // Selection Statement
            if(ans=='y')
            {
                break
            }
            else
            {
                continue
            }
        }
        println("What kind of pizza would you like?")
        // For Loop
        for(pizza in pizzas)
        {
            println("$pizza")
            pizzaChoice = pizza
        }
        pizzaChoice = readLine()
        // Switch Case
        when(pizzaChoice)
        {
            "Neapolitan" -> println("You ordered $pizzaChoice the pizza in honor Queen Margherita")
            "Greek" -> println("You ordered $pizzaChoice Feta, Spinach, and Olives make me feel like a true Greek")
            "Sushi" -> println("You ordered $pizzaChoice now we arn't missing anything")
            "Tomato Pie" -> println(" You ordered $pizzaChoice you are a true Italian-American")
            "St. Louis Style" -> println("You ordered $pizzaChoice it's like pizza on crackers")
        }
        println("Would you like to add extra toppings?\n(y/n)")
        ans = readLine()!![0]
        var count: Int = 0
        if(ans=='y')
        {
            println("Which would you like")
            // For each
            toppings.forEach {print("Options ($count+1) $it, ");count++}
            println("\npress 0 to exit")
            while(ansNum!=0){
                // Not null check
                ansNum = readLine()!!.toInt()
                if (ansNum==0)
                    break
                choosen.add(toppings.elementAt(ansNum-1))
                println("You have choosen ")
                choosen.forEach {print("$it, ")}
                println()
            }
        }
        // Consuming extension function
        print("You have choosen ${pizzaChoice!!.yellPizza()} pizza with ")
        choosen.forEach { print("$it, ") }
    }

    // Extension function
    fun String.yellPizza(): String {
        return this.toUpperCase()+"!!!"
    }