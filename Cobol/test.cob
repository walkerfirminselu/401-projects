
					*> Test File I/O: READ, WRITE
					*> An example program which *>    reades (TFileIn.dat) records from a input file,
					*>    displays the records, 
					*>    and writes (TFileOut.dat)them to a sequential file.
					*>
					*> Progrm-ID: TFile.cob
					*> Author:    Kuo-pao Yang
					*> OS:        Ubuntu 18
					*> Compiler:  OpenCOBOL
					*> Note:
					*> The following instructions are used to
					*>       edit, compile, and run this program
					*>    $ nano  TFile.cob
					*>    $ cobc -x -free TFile.cob
					*>    $ ./TFile

					IDENTIFICATION DIVISION.
					PROGRAM-ID.  TFile.
                    *> Used to specify I/O files
					ENVIRONMENT DIVISION.
					INPUT-OUTPUT SECTION.
					FILE-CONTROL.
                        SELECT myInFile  ASSIGN TO "P2Out.dat" ORGANIZATION IS LINE SEQUENTIAL.
						SELECT myOutFile ASSIGN TO "P2In.dat" ORGANIZATION IS LINE SEQUENTIAL.
					DATA DIVISION.
                    *> Used to describe record structure of the file
					FILE SECTION.
					FD myInFile.
                    *> syntax: Level number,    Data name,   Picture Clause,   Value Clause
                    *> 01          for Record description entry
                    *> 02-49       for Group and Elementary items
                    *> 66          for Rename Clause item
                    *> 77          for Items that can not be subdivided
                    *> 88          for common name entry
                    *> Common Verbs:   Accept, Display, Initialize, Move, Add, Subtract, Multiply, Divide, Compute
                    *> Usage:          Multiply A BY B GIVING C
                    *>                 Add A TO C GIVING C
                    *>                 Add A TO C
                    *>                 Compute A + (B * C) + WS-NUM-3 
                    *> Get user in     ACCEPT WS-ACCEPT.
                    *> Get system in   ACCEPT WS-ACCEPT FROM TIME.
                    *> Loops  perform  Thru, Until, Times, Varying
                                    *>    Go To
					01 recordIn.
					    05  Course    PIC X(10).
                        05  FILLER    PIC X.
					    05  wNumber   PIC X(10).
                        05  FILLER    PIC X.
                        05  semester  PIC X(11).
                        05  FILLER    PIC X.
                        05  className PIC X(30). 
                        05  FILLER    PIC X.
                        05  desc      PIC X(28).
                        05  FILLER    PIC X.
                        05  letterGrade     PIC X.
                        05  FILLER    PIC X.
                        05  grade     PIC X(4).
					FD myOutFile.
					01 outRecord.
                        05  scanline  PIC X(180).
            *>    Stores temporary files used in the program
					WORKING-STORAGE SECTION.
					01 w   PIC  X(3)  VALUE "YES".
                    01 WS-UNI-NAME-TOP.
                        05 FILLER      PIC X(20).
                        05 WS-UNI-NAME PIC X(34) VALUE "SOUTHEASTERN LOUISIANNA UNIVERSITY".
                        05 FILLER      PIC X(126).
                    01 WS-UNI-ADD-TOP. 
                        05 FILLER      PIC X(30).
                        05 WS-UNI-ADD  PIC X(11) VALUE "HAMMOND, LA".
                        05 FILLER      PIC X(139).
                    01 NEW-LINE.
                        05 FILLER PIC X(180).
                    01 WS-TABLE-TITLE.
                        05 WS-COURSE PIC X(6) VALUE "COURSE".
                        05 FILLER    PIC X(5).
                        05 WS-TITLE  PIC X(5) VALUE "TITLE".
                        05 FILLER    PIC X(20).
                        05 WS-GRADE  PIC X(2) VALUE  "GR".
                        05 FILLER    PIC X(5).
                        05 WS-EARNED PIC X(6) VALUE "EARNED".
                        05 FILLER    PIC X(5).
                        05 WS-QPT    PIC X(4) VALUE "QPTS".
                    *> Stores files that are allocated and initialized at program start
                    local-storage section.

                   *>    Describes variables from external program
                    linkage section.
                    
                    
					PROCEDURE DIVISION.
						OPEN INPUT myInFile.
						OPEN OUTPUT myOutFile.
						PERFORM subRead
                            MOVE WS-UNI-NAME-TOP to scanline
                            WRITE outRecord
                            MOVE WS-UNI-ADD-TOP to scanline
                            WRITE outRecord
                            MOVE NEW-LINE to scanline
                            WRITE outRecord
						PERFORM UNTIL w = "NO"
                            MOVE WS-TABLE-TITLE to scanline
                            WRITE outRecord
						    MOVE Course to scanline(1:20)
						    MOVE wNumber to scanline(21:8)
                            MOVE semester to scanline(29:11)
                            MOVE className to scanline(41:7)
                            MOVE desc to scanline(49:28)
                            MOVE letterGrade to scanline(76:1)
                            MOVE grade to scanline(78:4)
						    WRITE outRecord
						   PERFORM subRead
						END-PERFORM.
						CLOSE myInFile.
						CLOSE myOutFile.
						STOP RUN.
					subRead.
						READ myInFile NEXT RECORD INTO recordIn
						   AT END MOVE "NO" TO w
						   NOT AT END
							  DISPLAY "imer"
						END-READ.

			
