#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int control = 1;
char str[100];
char subStrMydir[100];
char subStrMydirDir[100];
char lscommand[100];
int nom;
extern char **environ;

void close();
void substring();

int main() {
printf("You may enter your commands\n(myquit, myclear, clear, myenviron, <your system command>)\n");
	while (control){
		gets(str);
		substring(str,subStrMydir,1,5);
		substring(str,subStrMydirDir,7,sizeof(subStrMydir)/sizeof(subStrMydir[0])-7);
		strcpy(lscommand,"ls -la ");
		strcat(lscommand, subStrMydirDir);
		char ** env = environ;
		if(!strcmp(str,"myquit"))
		{
			close();
		}
		else if(!strcmp(str,"myclear"))
		{
			system("clear");
		}
		else if(!strcmp("mydir",subStrMydir))
		{
			system(lscommand);
		}
		else if(!strcmp(str,"myenviron"))
		{
			while(*env)
			printf("%s\n",*env++);
		}
		else{
			system(str);
		}
	}
	return 0;
}

void close(){
	control=0;
}

void substring(char s[], char sub[], int p, int l) {
   int c = 0;
   
   while (c < l) {
      sub[c] = s[p+c-1];
      c++;
   }
   sub[c] = '\0';
}
